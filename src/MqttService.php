<?php

namespace Drupal\simple_mqtt;
require_once dirname(__DIR__) . '/lib/vendor/autoload.example.php';
use \sskaje\mqtt\MQTT;
use \sskaje\mqtt\Debug;

use Drupal\Core\Database\Driver\mysql\Connection;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Session\AccountProxy;
/**
 * Class MqttService.
 *
 * @package Drupal\mqtt
 */
class MqttService implements MqttServiceInterface {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;
  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;
  /**
   * Drupal\Core\Logger\LoggerChannelFactory definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;
  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;
  /**
   * Constructor.
   */
  public function __construct(Connection $database, ConfigManager $config_manager, LoggerChannelFactory $logger_factory, AccountProxy $current_user) {
    $this->database = $database;
    $this->configManager = $config_manager;
    $this->loggerFactory = $logger_factory;
    $this->currentUser = $current_user;
  }
  public function sendMsg($topic,$msg) {
    $config = $this->configManager->getConfigFactory()->get('mqtt.Setting');
    $address = $config->get('address');
    $client = $config->get('client_name');
    $username = $config->get('user');
    $password = $config->get('passwd');
    $mqtt = new MQTT($address, $client);

    $mqtt->setVersion(MQTT::VERSION_3_1_1);
//    Debug::Enable();
    $mqtt->setKeepalive(60);
    $mqtt->setAuth($username, $password);
    $connected = $mqtt->connect();
    if ($connected) {
      $mqtt->publish_async($topic, $msg, 0);
      $mqtt->disconnect();
   }
  }

}
