<?php

namespace Drupal\simple_mqtt;

/**
 * Interface MqttServiceInterface.
 *
 * @package Drupal\mqtt
 */
interface MqttServiceInterface {

  public function sendMsg($topic,$msg);

}
