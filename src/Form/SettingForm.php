<?php

namespace Drupal\simple_mqtt\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingForm.
 *
 * @package Drupal\mqtt\Form
 */
class SettingForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'mqtt.Setting',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'setting_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('mqtt.Setting');

    $form['address'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('MQTT server address'),
      '#description' => $this->t('MQTT server address( for Apollo server, it is tcp://host:port/,for example tcp://182.92.174.162:61613/)'),
      '#default_value' => $config->get('address'),
      '#maxlength' => 50,
      '#size' => 64,
    );
    $form['client_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Client Name'),
      '#description' => $this->t("this drupal name"),
      '#maxlength' => 50,
      '#size' => 64,
      "#default_value" => $config->get('client_name'),
    );
    $form['user'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('login user Name'),
      '#description' => $this->t("the login user name"),
      '#maxlength' => 50,
      '#size' => 64,
      "#default_value" => $config->get('user'),
    );
    $form['passwd'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('login password'),
      '#description' => $this->t("the login password"),
      '#maxlength' => 50,
      '#size' => 64,
      "#default_value" => $config->get('passwd'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('mqtt.Setting');
    $config->set('address', $form_state->getValue('address'));
    $config->set('client_name',$form_state->getValue('client_name'));
    $config->set('user', $form_state->getValue('user'));
    $config->set('passwd', $form_state->getValue('passwd'));

    $config->save();
  }

}
