INTRODUCTION
------------
 simple mqtt include a setting form, a service interface,when setting complete,developer can connect to message server and publish topic.
 
     if(\Drupal::hasService('mqtt.service')) {
       \Drupal::service('mqtt.service')->sendMsg('mytopic',$notify);
     }
.

INSTALLATION
------------
No special install steps are necessary to use this module, see https://www.drupal.org/documentation/install/modules-themes/modules-8 for further information.


MAINTAINERS
-----------
Current maintainers:

 * Umoney (https://www.drupal.org/u/umoney)
